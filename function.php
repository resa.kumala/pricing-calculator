<?php

function calculate_price($cost, $margin) {
  $price = $cost + ($cost * $margin);
  return $price;
}

$cost = 100;
$margin = 0.2;

$price = calculate_price($cost, $margin);

echo "The price is $price";

?>
